package Util;

import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

/**
 * Created by Abhijit on 13-12-2016.
 */
/*link for webservice = http://203.129.224.73:8080/geoserver*/
/*lat long values = 72.39440918, 23.59419433*/
public class TileProviderFactory {

    public static WMSTileProvider getOsgeoWmsTileProvider(String STR_WMS_LAYER_NAME) {


        //This is configured for:
        // http://beta.sedac.ciesin.columbia.edu/maps/services
        // (TODO check that this WMS service still exists at the time you try to run this demo,
        // if it doesn't, find another one that supports EPSG:900913
        final String WMS_FORMAT_STRING =
                GlobalVariable.WMSTILEPROVIDERURL + GlobalVariable.PORTNO + GlobalVariable.URLEXTENSION + //"http://49.248.112.146:8080/geoserver/wms" +
                        "?service=WMS" +
                        "&version=1.1.1" +
                        "&request=GetMap" +
                        "&layers=" + STR_WMS_LAYER_NAME +
                        "&bbox=%f,%f,%f,%f" +
                        "&width=256" +
                        "&height=256" +
                        "&srs=EPSG:900913" +
                        "&format=image/png" +
                        "&transparent=true";
// http://49.248.112.146:8080/geoserver/wms?service=WMS&version=1.1.1&request=GetMap&layers=estate_boundary_38576&bbox=8063800.738027,2664900.554813,8064412.234253,2665512.051040&width=256&height=256&srs=EPSG:900913&format=image/png&transparent=true
// http://49.248.112.146:8080/geoserver/wmsservice=WMS&version=1.1.1&srs=EPSG:4326&bbox=72.44130559265614,23.275958986320017,72.44903068989515,23.288081180292604&styles=&&buffer=40&info_format=application/json&request=GetFeatureInfo&layers=sgl:estate_boundary_38576&query_layers=opengeo:estate_boundary_38576&width=720&height=1230&x=383&y=356
        Log.e("service link :",""+WMS_FORMAT_STRING);


        WMSTileProvider tileProvider = new WMSTileProvider(256,256)
        {

            @Override
            public synchronized URL getTileUrl(int x, int y, int zoom) {
                double[] bbox = getBoundingBox(x, y, zoom);
                String s = String.format(Locale.US, WMS_FORMAT_STRING, bbox[MINX],bbox[MINY], bbox[MAXX], bbox[MAXY]);
                Log.d("WMSDEMO", s);
                URL url = null;
                try {
                    url = new URL(s);
                } catch (MalformedURLException e) {
                    throw new AssertionError(e);
                }
                return url;
            }
        };
        return tileProvider;
    }
}
