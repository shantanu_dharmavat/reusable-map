package com.example.hardcastleweb.mapwmsreusable;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button buttonMap,buttonDayIncident;
    Intent intent;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        MultiDex.install(this);
        String app = BuildConfig.APPLICATION_ID;
        Log.e("package name", "name " + app);
    }

    private void init() {
        buttonMap = (Button) findViewById(R.id.buttonMap);

        setClickListeners();
    }

    private void setClickListeners() {
        buttonMap.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.buttonMap:
                intent = new Intent(MainActivity.this,Test1.class);
                startActivity(intent);
                break;
        }
    }
}
