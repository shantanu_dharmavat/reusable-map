package com.example.hardcastleweb.mapwmsreusable;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import Util.GlobalVariable;
import Util.MapPopUp_ListAdaptor;
import Util.TileProviderFactory;

import static com.example.hardcastleweb.mapwmsreusable.R.id.map;

/**
 * Created by SHANTANU on 06/01/17.
 */
public class Test1 extends ActualMapActivity implements OnMapReadyCallback {

    //public static String layerStatic = "estate_boundary_38576";
    public static String workspacePrefix = "opengeo";
    public static String[] vvv_lay0_key_el = {"Estate Boundary" };
    public static String[] vvv_lay0_value = {"estate_boundary_38576"};
    public static boolean[] vvv_lay0_bol = {true};//{, false, false}

    private Boolean GPS_CURRENT_LOCATION = false;

    // constant ==============================
    public static String[] selected_key;//= vvv_lay2_key.clone();
    public static String[] selected_value;//=vvv_lay2_value.clone();
    public static boolean[] set_visibility;// =vvv_lay2_bol.clone();

    String str_popup_arr[];
    ArrayList<String> final_pop_up_key = new ArrayList<>();
    ArrayList<String> final_pop_up_value = new ArrayList<>();
    String str_popup_arr_key[];
    String str_popup_arr_value[];
    //layer list end ==============================


    private ProgressDialog mProgressDialog;
    TileOverlay[] tileOverlay;
    TileProvider[] wmsTileProvider;
    private GoogleMap mMap;

    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;

    CameraPosition cameraPosition;
    double db_left = 0.0;
    double db_top = 0.0;
    double db_right = 0.0;
    double db_bottom = 0.0;
    Point clicked_xy;
    ImageButton layer_stack, layer_info, toggle_gps;
    int selected_layer_info = 0;
    static int selected_mapLayer = 0;
    int map_width = 0;
    int map_height = 0;
    String str_pop_up;
    private int PERMISSION_CODE_1 = 23;
    private LocationManager lm;
    private String TAG = "TEST 1";

    Boolean fromPushNotification = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "OnCreate");
        if (getIntent().hasExtra("incident"))
            fromPushNotification = getIntent().getExtras().getBoolean("incident");
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        initiateGooglePlayService();
        setLayer();
//        setUpMapIfNeeded();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void startDemo() {
        Log.e(TAG, "startDemo");
        layer_stack = (ImageButton) findViewById(R.id.layer_stack);
        layer_info = (ImageButton) findViewById(R.id.layer_info);
        toggle_gps = (ImageButton) findViewById(R.id.toggle_gps);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.e("INSIDE", "onConnected");
        Log.i(TAG, "Connected to GoogleApiClient");

        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        }
        setUpMap();
    }



    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        permissionRequest();
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

        if (location.hasBearing()) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            cameraPosition = new CameraPosition.Builder()
                    .target(latLng)
                    .zoom(12)
                    .bearing(location.getBearing())
                    .tilt(0)
                    .build();

            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void setLayer() {
        switch (Util.GlobalVariable.MAP_ID) {

            case 0:
                selected_key = vvv_lay0_key_el.clone();
                selected_value = vvv_lay0_value.clone();
                set_visibility = vvv_lay0_bol.clone();
                break;
        }
        tileOverlay = new TileOverlay[selected_value.length];
        wmsTileProvider = new TileProvider[selected_value.length];
    }

    @Override
    public void onMapReady(GoogleMap map) {
        super.onMapReady(map);
        mMap = map;
        // Check if we were successful in obtaining the map.
        setUpMap();
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(GlobalVariable.DEFAULTLAT, GlobalVariable.DEFAULTLONG));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);

        permissionRequest();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionRequest();
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        LatLngBounds region = mMap.getProjection().getVisibleRegion().latLngBounds;

        /*if (fromPushNotification) {
            Double lat = Double.valueOf(GlobalVariable.getInstance().notificationModelList.get(0).getLatitude());
            Double lon = Double.valueOf(GlobalVariable.getInstance().notificationModelList.get(0).getLongitude());
            String titleString = GlobalVariable.getInstance().notificationModelList.get(0).getTitle();
            MarkerOptions marker = new MarkerOptions().position(new LatLng(lat, lon)).title(titleString);
            CameraUpdate center1 = CameraUpdateFactory.newLatLng(new LatLng(GlobalVariable.DEFAULTLAT, GlobalVariable.DEFAULTLONG));
            CameraUpdate zoom1 = CameraUpdateFactory.zoomTo(12);
            mMap.addMarker(marker).setDraggable(true);
            mMap.moveCamera(center1);
            mMap.animateCamera(zoom1);
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    str_popup_arr_key = GlobalVariable.getInstance().final_pop_up_key.toArray(new String[final_pop_up_key.size()]);
                    str_popup_arr_value = GlobalVariable.getInstance().final_pop_up_value.toArray(new String[final_pop_up_value.size()]);
                    custom_alert();
                    return false;
                }
            });
        }*/

        db_top = region.northeast.latitude;
        db_left = region.southwest.longitude;

        db_right = region.northeast.longitude;
        db_bottom = region.southwest.latitude;
    }

    public void layerSwitcher(View v) {
        switch (v.getId()) {
            case R.id.layer_stack:
                AlertDialog dialog1 = new AlertDialog.Builder(this)
                        .setTitle("Select a category to display")
                        .setMultiChoiceItems(selected_key, set_visibility, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                                if (isChecked) {
                                    tileOverlay[indexSelected].setVisible(true);// = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(wmsTileProvider[indexSelected]));
                                    //Log.e("><><>", "checked index " + indexSelected);

                                } else {

                                    tileOverlay[indexSelected].setVisible(false);
                                    //Log.e("><><>", "unchecked  checked index " + indexSelected);
                                }
                            }
                        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        //  Your code when user clicked on OK
                                    }
                                }
                        ).create();
                dialog1.show();
                break;
            case R.id.layer_info:
                // int selectedElement=-1; //global variable to store state
                AlertDialog dialog2;
                //final String[] selectFruit= new String[]{"Blacklist","Whitelist"};

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose a category for information");
                builder.setSingleChoiceItems(selected_key, selected_layer_info,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                selected_layer_info = which;
                                // Toast.makeText(Test1.this, GlobalVariable.selected_key[which]+":"+ which + " Selected", Toast.LENGTH_LONG).show();
                                //  dialog.dismiss();
                            }
                        });
                builder.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                dialog2 = builder.create();
                dialog2.show();
                break;

            case R.id.toggle_gps:
                GPS_CURRENT_LOCATION = true;
                gotoLocation();
                break;

            case R.id.imageViewDirection:
                Log.e("direction Button", "Clicked");
                GlobalVariable globalVariable = GlobalVariable.getInstance();
                globalVariable.setmGlobalCurrentLocation(mCurrentLocation);
                Intent intent = new Intent(Test1.this, DirectionMap.class);
                startActivity(intent);
                break;

            case R.id.imageViewMapLayer:
                AlertDialog dialogMap;
                //final String[] selectFruit= new String[]{"Blacklist","Whitelist"};
                final String[] mapLayerList = new String[]{"Satellite","Terrain","Hybrid"};
                AlertDialog.Builder builderMap = new AlertDialog.Builder(this);
                builderMap.setTitle("Choose a Map Layer");
                builderMap.setSingleChoiceItems(mapLayerList, selected_mapLayer,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                selected_mapLayer = which;
                                setUpMap();
                                // Toast.makeText(Test1.this, GlobalVariable.selected_key[which]+":"+ which + " Selected", Toast.LENGTH_LONG).show();
                                //  dialog.dismiss();
                            }
                        });
                builderMap.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                dialogMap = builderMap.create();
                dialogMap.show();
                break;

        }
    }

    private void setUpMap() {
        for (int i = 0; i < selected_value.length; i++) {
            wmsTileProvider[i] = TileProviderFactory.getOsgeoWmsTileProvider(selected_value[i]);
                    tileOverlay[i] = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(wmsTileProvider[i]));
            if (set_visibility[i] == true)
                tileOverlay[i].setVisible(true);
            else
                tileOverlay[i].setVisible(false);
        }
//        wmsTileProvider[0] = TileProviderFactory.getOsgeoWmsTileProvider(layerStatic);

        if (selected_mapLayer == 0) {
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);//MAP_TYPE_TERRAIN
        }else if (selected_mapLayer == 1){
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);//MAP_TYPE_SATELLITE
        }else if (selected_mapLayer == 2){
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                onMapClickMethod(latLng);
            }
        });
    }

    private void gotoLocation() {
        try {
            if (GPS_CURRENT_LOCATION) {
                Log.e("inside location", "gpl location");
                if (mMap != null) {

                    final GoogleMap.OnMyLocationChangeListener mCurrentLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
                        @Override
                        public void onMyLocationChange(Location location) {
                            mCurrentLocation = location;
                        }
                    };
                    permissionRequest();
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mCurrentLocation = getLastKnownLocation();
                        if (mCurrentLocation != null) {
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(Criteria.ACCURACY_FINE);
                            LatLng latLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                            mMap.setMyLocationEnabled(true);
                            mMap.getUiSettings().setCompassEnabled(true);
                            mMap.setOnMyLocationChangeListener(mCurrentLocationChangeListener);
                            GPS_CURRENT_LOCATION = false;
                        } else {
                            Log.e("mCurrentLocation", "null");
                        }
                    } else {
                        Log.e("permission", "not set");
                    }

                } else {
                    Log.e("mMap", "is null");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void permissionRequest() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissionsFine();
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissionsCoarse();
        }
    }

    public void requestPermissionsFine() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_CODE_1);
    }

    public void requestPermissionsCoarse() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_CODE_1);
    }

    public void onMapClickMethod(LatLng point) {
        SupportMapFragment mMapFragment1 = (SupportMapFragment) (getSupportFragmentManager().findFragmentById(map));
//        SupportMapFragment mMapFragment1 = (SupportMapFragment) mMapFragment1.getMapAsync();
        map_width = mMapFragment1.getView().getWidth();
        map_height = mMapFragment1.getView().getHeight();

        LatLngBounds region = mMap.getProjection().getVisibleRegion().latLngBounds;
        db_left = region.southwest.longitude;
        db_top = region.northeast.latitude;
        db_right = region.northeast.longitude;
        db_bottom = region.southwest.latitude;
        clicked_xy = mMap.getProjection().toScreenLocation(point);
        new getFeatureInfo().execute();

    }

    private void initiateGooglePlayService() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private class getFeatureInfo extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(Test1.this);
            mProgressDialog.setMessage("Fetching data .. ");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                URL url;
                url = new URL(GlobalVariable.WMSTILEPROVIDERURL + GlobalVariable.PORTNO + GlobalVariable.URLEXTENSION + //  /wms
                        "?service=WMS&" +
                        "version=1.1.1&" +
                        "srs=EPSG:4326&" +
                        "bbox=" + db_left + "," + db_bottom +
                        "," + db_right + "," + db_top + "&" +
                        "styles=&" +
                        "&buffer=40&" +
                        "info_format=application/json&" +
                        "request=GetFeatureInfo&" +
                        "layers=" + workspacePrefix + ":" + selected_value[selected_layer_info] + "&" +
                        "query_layers=" + workspacePrefix + ":" + selected_value[selected_layer_info] + "&" +//geopolis //selected_value[selected_layer_info]
                        "width=" + map_width + "&" +
                        "height=" + map_height + "&" +
                        "x=" + clicked_xy.x + "&" +
                        "y=" + clicked_xy.y);

                Log.e(">>>>>>>>>>>>>>>>>>", " " + url.toString());
                URLConnection conection = url.openConnection();
                conection.connect();
                // Get Music file                                                                                                                                                                                              length
                int lenghtOfFile = conection.getContentLength();
                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 10 * 1024);
                // Output stream to write file in SD card
                // Convert response to string
                try {
                    //Log.e("log_tag 123", "Error converting result ");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    input.close();
                    str_pop_up = sb.toString();
                    //Log.e(">>>>>>>>>>>>>>>>>>", " " + str_pop_up.toString());

                } catch (Exception e) {
                    Log.e("log_tag 862", "Error converting result " + e.toString());
                }

            } catch (Exception e) {
                Log.e("Error 184: ", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            //TextView content = new TextView(Test1.this);
            //content.setText("" + str_pop_up);
            Log.e("geojson ", ">  " + str_pop_up);
            try {
                if (str_pop_up != null) {//|| !str_pop_up.isEmpty()
                    JSONObject json = new JSONObject(str_pop_up);
                    JSONArray features = new JSONArray();

                    if (json.length() > 0) {
                        //JSONArray properties = new JSONArray();

                        for (Iterator<String> iter = json.keys(); iter.hasNext(); ) {
                            String key = iter.next();
                            //String value = (String) json.get(key);
                            Log.e("geojson ", ">  " + key);
                        }

                        if (json.has("features")) {
                            features = json.getJSONArray("features");
                            //Log.e("log Val ", "=====  ");

                        } else {
                            str_popup_arr = new String[str_popup_arr.length];
                        }
                        //Log.e("Length :"," >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+features.length());

                        if (features.length() == 0) {
                            //str_popup_arr = new String[str_popup_arr.length];
                            str_popup_arr = new String[0];
                        }
                        final_pop_up_key.clear();
                        final_pop_up_value.clear();

                        boolean data_there = false;

                        for (int i = 0; i < features.length(); ++i) {
                            JSONObject rec = features.getJSONObject(i);
                            //int id = rec.getInt("id");
                            // properties = json.getJSONArray("properties");
                            String str_propertie = rec.getString("properties");
                            JSONObject properties = new JSONObject(str_propertie);
                            for (Iterator<String> iter = properties.keys(); iter.hasNext(); ) {
                                String key = iter.next();
                                String value = properties.get(key).toString();

                                Log.e("geojson Greece  ", "Key:  " + key + " Value : " + value);
                                if (!value.equalsIgnoreCase("null"))// do not accept the null values
                                {
                                    final_pop_up_key.add(key);
                                    final_pop_up_value.add(value);
                                }
                            }
                            data_there = true;
                            Log.e(" count --->> split  >> ", " ");
                            //str_popup_arr = str_propertie.split(",");
                            //Log.e("data :"," "+str_propertie);
                            //str_popup_arr = str_propertie.split(":");
                        }
                        if (data_there) {
                            str_popup_arr_key = final_pop_up_key.toArray(new String[final_pop_up_key.size()]);
                            str_popup_arr_value = final_pop_up_value.toArray(new String[final_pop_up_value.size()]);
                            custom_alert();
                        } else {
                            Toast.makeText(Test1.this, "No data at this location ", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(Test1.this, "No Data for this location", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //alert_pop_up();
            //ShowAlertDialogWithListview();

        }

    }

    public void custom_alert() {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Information List");
        View view = getLayoutInflater().inflate(R.layout.map_pop_up_layout, null);
        ListView lv = (ListView) view.findViewById(R.id.map_pop_up_lv);
        // Change MyActivity.this and myListOfItems to your own values
        MapPopUp_ListAdaptor clad = new MapPopUp_ListAdaptor(Test1.this, str_popup_arr_key, str_popup_arr_value);
        lv.setAdapter(clad);
        //lv.setOnItemClickListener(........);
        dialog.setContentView(view);
        dialog.show();
    }

    private Location getLastKnownLocation() {
        List<String> providers = lm.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissionsFine();
            } else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissionsCoarse();
            }
            Location l = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//            Location l =  lm.getLastKnownLocation(provider);
            if (l == null) {
                Log.e("inside getLastLocation", "lm = null");
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    protected int getLayoutId() {
        return R.layout.activity_test1;
    }

}
